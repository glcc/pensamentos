//
//  Quote.swift
//  Pensamentos
//
//  Created by Gerson  on 14/09/2018.
//  Copyright © 2018 Gerson . All rights reserved.
//

import Foundation

struct Quote: Codable /*Encodable, Decodable*/ {
    let quote: String
    let author: String
    let image: String
    
    var quoteFormatted: String {
        return " 〝" + quote + "〞 "
    }
    
    var authorFormatted: String {
        return "- " + author + " -"
    }
}
